import './vendor';
$(document).ready(function(){
	if ($(window).width() > 980) {
		$('.product__inner-wrapper').mouseleave(function(){
			if($(this).prev().is(':checked')){
				$(this).addClass('show-sub');
			}
		});
	}

	$('.productSelect').change(function() {
		if(!this.checked) {
			$(this).next().removeClass('show-sub');
		}
	});
	$('.product__afterText label').on('click', function(){
		$(this).closest('.product').find($('.product__inner-wrapper')).addClass('show-sub');
	});
});

